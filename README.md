# ProcessingClock

A neat clock written in Java using Processing.

Each hand is a 'Time' Object and the Clock class is the main.

![](https://gitlab.com/willmac321/ProcessingClock/raw/master/ClockScSh.PNG)